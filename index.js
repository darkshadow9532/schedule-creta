const express = require('express');
const path = require('path')
const bodyParser = require('body-parser');

const PORT = process.env.PORT || 5000;
//const PORT = 5000;
// create express app
const app = express();

//Declare moment
var moment = require('moment');
moment.locale("vi");
moment().format();

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())

// Configuring the database
//const dbConfig = require('./config/database.config.js');
// const mongoose = require('mongoose');

// mongoose.Promise = global.Promise;

// // Connecting to the database
// console.log(dbConfig.url);
// mongoose.connect(dbConfig.url, {
//     useNewUrlParser: true,
//     useUnifiedTopology: true
// }).then(() => {
//     console.log("Successfully connected to the database");    
// }).catch(err => {
//     console.log('Could not connect to the database. Exiting now...', err);
//     process.exit();
// });

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const uri = "mongodb+srv://darkshadow9532:minh9532@cluster0-auyfy.mongodb.net/test?retryWrites=true&w=majority";
mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log("Successfully connected to the database");
    // mongoose.close();
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

var route = require("./route");
// define a simple route

app.use('/static', express.static('public'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs')
app.get('/', route.main);

// Require Notes routes
require('./app/routes/work.routes.js')(app);
require('./app/routes/mqtt.routes.js')(app);
require('./app/routes/schedule.routes.js')(app);
require('./app/routes/template.routes.js')(app);
require('./app/routes/work_template.routes.js')(app);

// listen for requests
app.listen(PORT, () => {
    console.log("Server is listening on port"+ PORT);
    console.log(uri);
});

const mqtt = require('async-mqtt');

var server  = mqtt.connect('mqtt://cretabase.kbvision.tv:1883');

server.on('connect', function () {
    server.subscribe('minh', function (err) {
        if(!err) {
            client.publish('minh','lo cc');
        }
    })
})

server.on('message', function (topic, message) {
    // message is Buffer
    console.log(message.toString())
    //server.end()
});

const Works = require('./app/models/work.model.js');
const MQTTs = require('./app/models/mqtt.model.js');
function deadline_check(){
    console.log("Starting...")
    let query_work = Works.find();
    let query_mqtt = MQTTs.find();
    let promise_work = query_work.exec();
    let promise_mqtt = query_mqtt.exec();
    Promise.all(
        [
            Promise.resolve(promise_work),
            Promise.resolve(promise_mqtt)
        ]
    )
    .then(function(result){
        //console.log(result);
        let works = result[0];
        let works_true = [];
        for (let i in works){
            if (Math.round(moment().diff(works[i].time, 'minutes', true)) === 0){
                works_true.push(works[i]);
            }
        }
        console.log("works_true:", works_true);
        for (let i in works_true){
            //console.log("mqtt");
            // client.on('connect', function () {
            //     console.log("mqtt-connect");
            //     client.subscribe('presence', function (err) {
            //         if (!err) {
            //             console.log("mqtt-send");
            //             client.publish('presence', 'Hello mqtt')
            //         }
            //     })
            // })
            let mqttId = works_true[i].action;
            var mqtt_client;
            let mqtts = result[1];
            console.log("mqttId",mqttId);
            //  console.log("mqtts", mqtts);

            
            for (let j in mqttId){
                for (let k in mqtts){
                    if (mqtts[k]._id == mqttId[j]){
                        let client_info = "";
                        console.log(mqtts[k].port);
                        if (mqtts[k].port != ""){
                            
                            client_info = "mqtt://" + mqtts[k].host + ":" + mqtts[k].port;

                        }
                        else {
                            client_info  = "mqtt://" + mqtts[k].host;
                        }
                        console.log(client_info);
                        let client  = mqtt.connect(client_info);
                        //console.log(client);
                        client.on('connect', function () {
                            console.log('connect');                            
                            client.publish(mqtts[k].topic, mqtts[k].message);
                            console.log(mqtts[k].topic, mqtts[k].message);
                            client.end();
                        });
                    }
                }             
                    
            }
            
            // client.on('message', function (topic, message) {
            // // message is Buffer
            //     console.log(message.toString());
            //     client.end();
            // })
            // for (let j in mqttId){
            //     //console.log("mqtt");
            //     if (mqtts.includes(mqttId[j])){
            //         mqtt_client  = await mqtt.connectAsync('mqtt://test.mosquitto.org')
            //         // mqtt_client.subscribe(mqttId[j].topic, function (err) {
            //         //     if (!err) {
            //         //         console.log("mqtt-send");
            //         //         client.publish('presence', mqttId[j].message);
            //         //         console.log(mqttId[j].message);
            //         //     }
            //         //     else{
            //         //         console.log(err);
            //         //     }
            //         // })
            //         try {
            //             console.log("mqtt-send");
            //             await client.publish(mqttId[j].topic, mqttId[j].message);
            //         }
            //         catch (e){
            //             // Do something about it!
            //             console.log(e.stack);
            //             process.exit();
            //         }
            //     }                    
            // }         
        }
    })
    // .then(works => {
    //     let works_true = [];
    //     for (let i in works){
    //         if (moment().diff(works[i].time,'minutes') == 0){
    //             works_true.push(works[i]);
    //         }
    //     }
    //     console.log(works_true);
    // })
    // .then(works_true =>{
    //     for (let i in works_true){
    //         console.log("mqtt");
    //         // client.on('connect', function () {
    //         //     console.log("mqtt-connect");
    //         //     client.subscribe('presence', function (err) {
    //         //         if (!err) {
    //         //             console.log("mqtt-send");
    //         //             client.publish('presence', 'Hello mqtt')
    //         //         }
    //         //     })
    //         // })
    //         let mqttId = works_true[i].action;
    //         var mqtt_client;
    //         MQTTs.find()
    //         .then(mqtts => {
    //             for (let j in mqttId){
    //                 if (mqtts.includes(mqttId[j])){
    //                     mqtt_client  = mqtt.connect('mqtt://test.mosquitto.org')
    //                     mqtt_client.subscribe(mqttId[j].topic, function (err) {
    //                         if (!err) {
    //                             console.log("mqtt-send");
    //                             client.publish('presence', mqttId[j].message);
    //                             console.log(mqttId[j].message);
    //                         }
    //                     })
    //                 }                    
    //             }
    //         })
    //         .catch(err => {
    //             // res.status(500).send({
    //             //     message: err.message || "Some error occurred while retrieving notes."
    //             // });
    //         });           
    //     }
    .catch(err => {
        console.log(err);
        // res.status(500).send({
        //     message: err.message || "Something went wrong"
        // });
    });
}

setInterval(deadline_check, 60000);