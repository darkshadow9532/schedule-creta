import { MqttClient } from "mqtt";

moment.locale("vi");
moment().format();
$(document).ready(function(){
    $("#page-1").css("display", "none");
    $("#page-2").css("display","none");
    //$("#page-3").css("display","none");
    $("#page-4").css("display","none");
    // $("#create-work-template").css("display","none");

    $("#add-template").click(function(){
        $("#page-4").css("display","none");
        $("#page-3").css("display","none");
        $("#page-2").css("display","none");
        $("#page-1").css("display","initial");
    });

    $("#add-schedule").click(function(){
        $("#page-4").css("display","none");
        $("#page-3").css("display","none");
        $("#page-2").css("display","initial");
        $("#page-1").css("display","none");
    });

    $("#add-work").click(function(){
        $("#page-4").css("display","none");
        $("#page-3").css("display","initial");
        $("#page-2").css("display","none");
        $("#page-1").css("display","none");
    });

    $("#add-work-template").click(function(){
        console.log("heelo");
        $("#page-4").css("display","block");
        $("#page-3").css("display","none");
        $("#page-2").css("display","none");
        $("#page-1").css("display","none");
    });

});

var app = angular.module("schedule",[]);

var url_work = window.location.origin + "/works";
var url_schedule = window.location.origin + "/schedules";
var url_template = window.location.origin + "/templates";
var url_work_template = window.location.origin + "/work_templates";
var url_mqtt = window.location.origin + "/mqtts";

var userID = "1412291";

var w_t = new WorkTemplate();
var w = new Work();
var m = new Mqtt();

var work_templates = [];
var works = [];
var works_display = [];
var work_template_display = [];

function create_work_display(works){
    works_display = works;
}

async function setup(){
    work_templates = await w_t.find();
    works = await w.find();
    mqtts = await m.find();
    works_display = create_work_display(works, mqtts);
}

async function add_new_work_template(work_template){
    let wT = new WorkTemplate(work_template);
    await wT.begin();
    await wT.save();
    return await wT.find();
}

async function add_new_work(work){
    let wT = new Work(work);
    await wT.begin();
    await wT.save();
    return await wT.find();
}


setup();
app.controller("schedule-ctrl", function($scope, $http, $interval, $timeout){
    
    $http.get(url_work)
    .then(function(res){
        $scope.works = res.data;
        console.log($scope.works);
    });
    $http.get(url_work_template)
    .then(function(res){
        $scope.work_templates = res.data;
        console.log($scope.work_templates);
    });
    $http.get(url_schedule)
    .then(function(res){
        $scope.schedules = res.data;
        console.log($scope.schedules);
    });
    $http.get(url_template)
    .then(function(res){
        $scope.templates = res.data;
        console.log($scope.templates);
    });
    $http.get(url_mqtt)
    .then(function(res){
        $scope.mqtts = res.data;
        console.log($scope.mqtts);
    });


    var works = new Work();
    works.begin();
    works.find();

    $scope.work_select = [];
    $scope.template = {
        queue: [],
        parent: "staff",
        name: ""
    }

    $scope.add_work_to_template = function (){
        $scope.template.queue.push($scope.work_select);
    }

    $scope.create_new_work = function (){
        $("#create-work-template").css("display","initial");
        console.log("Hello");
    }

    // Work

    $scope.work_mqtt_select = "";
    $scope.work = {
        action: [],
        name: "",
        time: 0,
        parent: "staff",
        parentId: userID
    }
    
    $scope.add_work_action = function (mqtt){
        $scope.work.action.push(mqtt);
    }

    $scope.confirm_add_work = function (){
        console.log("hello")
        console.log($scope.work);
        add_new_work($scope.work).then(function(result){
            works = result;
            $scope.works = works;
            console.log("works: ", $scope.works);
        });
        // $http({
        //     method: "POST",
        //     url: url_work,
        //     headers: {
        //         "Content-Type": 'application/json'
        //     },
        //     data: $scope.work
        // })
        // .then(function(res){
        //     console.log(res.data);
        //     if((res.data.status != 500)  && (res.data.status != 404 ) ){
        //         $scope.template.queue.push(res.data._id);
        //         $scope.works.push(res.data);
        //         $scope.work = {
        //             action: [],
        //             name: "",
        //             time: 0,
        //             parent: "staff",
        //             parentId: userID
        //         }
        //         console.log($scope.works);
        //         //alert("Created new work!");
        //     }
        //     else {
        //         //alert("Err creating work");
        //     }
        // })

        //$("#create-work-template").css("display","none");
    }

    $scope.work_mqtt_select = "";
    $scope.works_display = works;
    // Work template

    $scope.work_template_mqtt_select = "";
    $scope.work_template = {
        action: [],
        name: "",
        time: 0,
        parent: "staff",
        parentId: userID
    }
    
    $scope.add_work_template_action = function (mqtt){
        $scope.work_template.action.push(mqtt);
    }

    $scope.confirm_add_work_template = function (){
        console.log($scope.work_template);
        add_new_work_template($scope.work_template).then(function(result){
            work_templates = result;
            $scope.work_templates = work_templates;
            console.log("work_template:", work_templates);
        });
        
        // $http({
        //     method: "POST",
        //     url: url_work,
        //     headers: {
        //         "Content-Type": 'application/json'
        //     },
        //     data: $scope.work
        // })
        // .then(function(res){
        //     console.log(res.data);
        //     if((res.data.status != 500)  && (res.data.status != 404 ) ){
        //         $scope.template.queue.push(res.data._id);
        //         $scope.works.push(res.data);
        //         $scope.work = {
        //             action: [],
        //             name: "",
        //             time: 0,
        //             parent: "staff",
        //             parentId: userID
        //         }
        //         console.log($scope.works);
        //         //alert("Created new work!");
        //     }
        //     else {
        //         //alert("Err creating work");
        //     }
        // })

        //$("#create-work-template").css("display","none");
    }

    // Schedule
    $scope.confirm_add_template = function (){
        $http({
            method: "POST",
            url: url_template,
            headers: {
                "Content-Type": "application/json"
            },
            data: $scope.template
        })
        .then(function(res){
            console.log(res.data);
            if((res.data.status != 500) && (res.data.status != 404)){
                $scope.templates.push(res.data);
                $scope.template = {
                    queue: [],
                    parent: "staff",
                    name: ""
                }
                alert("Successful creating new Template");
            }
            else {
                alert("Err creating Template");
            }
        })
    }
    $scope.schedule = {
        queue: [],
        time_start: 0,
        parent: "staff",
        name: ""
    }

    $scope.template_select = "";
    $scope.time_start_abs = new Date();

    $scope.add_template_to_schedule = function (){
        for (let i in $scope.templates ){
            if ($scope.templates[i]._id == $scope.template_select){
                $scope.schedule.queue = $scope.template[i].queue;
            }
        }
    }

    $scope.change_time_start = function (){
        $scope.schedule.time_start = moment().diff($scope.time_start_abs, "minutes");
    }

    var req_schedule = [];
    $scope.confirm_add_schedule = function (){
        $http({
            method: "POST",
            url: url_schedule,
            headers: {
                "Content-Type": "application/json"
            },
            data: $scope.schedule
        })
        .then(function(res){
            for (let i in $scope.schedule.queue){
                for (let j in $works){
                    if($scope.schedule.queue[i] == $works[j]._id){
                        req_schedule.push({
                            action: $scope.works[j].action,
                            time: $scope.works[j].time,
                            parent: "schedule",
                            parentId: res.data._id,
                            name: $scope.works[j].name
                        });
                    }
                }
            }
            for ( i in req_schedule){
                $http({
                    method: "POST",
                    url: url_work,
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: req_schedule[i]
                })
                .then(function(res_1){
                    console.log(res_1.data);
                    if(!res_1.data.status != 500){
                        $scope.works.push(res_1.data);
                        $scope.schedule.queue.push(res_1.data._id);
                    }
                    else {
                    }
                    if($scope.schedule.queue.length === req_schedule.length){
                        $http({
                            method: "PUT",
                            url: url_schedule,
                            headers: {
                                "Content-Type": "application/json"
                            },
                            data: $scope.schedule
                        })
                        .then(function(res_2){
                            if(!res_2.data.status){
                                alert("Successful create Schedule");
                                $scope.schedules.push(res_2.data);
                            }
                        })
                    }
                })
            }
        })        
    }
});