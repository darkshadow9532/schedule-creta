module.exports = (app) => {
    const templates = require('../controllers/template.controller.js');

    // Create a new Note
    app.post('/templates', templates.create);

    // Retrieve all templates
    app.get('/templates', templates.findAll);

    // Retrieve a single Note with noteId
    app.get('/templates/:templateId', templates.findOne);

    // Update a Note with noteId
    app.put('/templates/:templateId', templates.update);

    // Delete a Note with noteId
    app.delete('/templates/:templateId', templates.delete);
}