module.exports = (app) => {
    const work_templates = require('../controllers/work_template.controller.js');

    // Create a new Note
    app.post('/work_templates', work_templates.create);

    // Retrieve all work_templates
    app.get('/work_templates', work_templates.findAll);

    // Retrieve a single Note with noteId
    app.get('/work_templates/:work_templateId', work_templates.findOne);

    // Update a Note with noteId
    app.put('/work_templates/:work_templateId', work_templates.update);

    // Delete a Note with noteId
    app.delete('/work_templates/:work_templateId', work_templates.delete);
}