module.exports = (app) => {
    const schedules = require('../controllers/schedule.controller.js');

    // Create a new Note
    app.post('/schedules', schedules.create);

    // Retrieve all schedules
    app.get('/schedules', schedules.findAll);

    // Retrieve a single Note with noteId
    app.get('/schedules/:scheduleId', schedules.findOne);

    // Update a Note with noteId
    app.put('/schedules/:scheduleId', schedules.update);

    // Delete a Note with noteId
    app.delete('/schedules/:scheduleId', schedules.delete);
}