const mongoose = require('mongoose');

const TemplateSchema = mongoose.Schema({
    // title: String,
    // content: String
    queue: Array,
    parentId: String,
    name: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Template', TemplateSchema);