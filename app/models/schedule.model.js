const mongoose = require('mongoose');

const ScheduleSchema = mongoose.Schema({
    // title: String,
    // content: String
    queue: Array,
    parentId: String,
    time_start: Date,
    name: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Schedule', ScheduleSchema);