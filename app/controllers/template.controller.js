const Template = require('../models/template.model.js');
var moment = require('moment');
moment.locale('vi');
moment().format();
// Create and Save a new Note
exports.create = (req, res) => {
    if(!req.body.queue){
        return res.status(400).send({
            message: "Template queue of works can not be empty"
        })
    }
    
    const template = new Template({
        queue: req.body.queue || [],
        parentID: req.body.parentID || "",
        time_start: moment().add(req.body.time_start, 'm').toDate() || moment()
    })
    template.save()
    .then(data => {
        res.send(data);
    })
    .catch(err =>{
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Template"
        })
    })
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Template.find()
    .then(templates => {
        res.send(templates);
    })
    .catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving the Template"
        })
    });
};

// Find a single note with a noteId
exports.findOne = (req, res) => {
    Template.findById(req.params.templateId)
    .then (template =>{
        if (!template){
            return res.status(404).send({
                message: "Template not found with id"
            });
        }
        res.send(template);
    })
    .catch(err => {
        if (err.kind === "ObjectId"){
            return res.status(404).send({
                message: "Template not found with Id"
            });
        }
        return res.status(500).send({
            message: "Error retrieving template with Id"
        });
    })
}

exports.findBy = (req, res) => {
    Template.find(req.params)
    .then(templates => {
        if(!templates){
            return res.status(404).send({
                message: "Template not found"
            })
        }
        res.send(templates);
    })
    .catch(err => {
        res.status(500).send({
            message: err.message || "SSome error occurred while retrieving the Template"
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    if (!req.body.queue){
        return res.status(400).send({
            message: "Template queue of works can not be empty"
        });
    }
    Template.findByIdAndUpdate(req.params.templateId, {
        queue: req.body.queue || [],
        parentID: req.body.parentID || "",
        time_start: moment().add(req.body.time_start, 'm').toDate() || moment()
    }, {new: true})
    .then(template =>{
        if(!template){
            return res.status(404).send({
                message: "Template not found"
            })
        }
        res.send(template);
    })
    .catch(err =>{
        return res.status(500).send({
            message: "Error updating:" + err.message
        });
    });
};

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Template.findByIdAndRemove(req.params.templateId)
    .then(template =>{
        if(!template){
            return res.status(404).send({
                message: "Template not found, can not delete"
            })
        }
    })
};