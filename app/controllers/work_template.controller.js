const WorkTemplate = require('../models/work_template.model.js');
var moment = require('moment');
moment.locale('vi');
moment().format();
// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    console.log(req.body);
    if(!req.body.time) {
        return res.status(400).send({
            message: "WorkTemplate time can not be empty"
        });
    }

    // Create a Note

    const work_template = new WorkTemplate({
        action: req.body.action || [],
        time: req.body.time,
        parent: req.body.parent,
        parentId: req.body.parentId,
        name: req.body.name
    });

    // Save Note in the database
    work_template.save()
    .then(data => {
        res.send(data);
        console.log(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    WorkTemplate.find()
    .then(work_templates => {
        res.send(work_templates);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

// Find a single note with a noteId
exports.findOne = (req, res) => {
    WorkTemplate.findById(req.params.work_templateId)
    .then(work_template => {
        if(!work_template) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.work_templateId
            });            
        }
        res.send(work_template);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "WorkTemplate not found with id " + req.params.work_templateId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving note with id " + req.params.work_templateId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {

    // Validate Request
    if(!req.body.content) {
        return res.status(400).send({
            message: "WorkTemplate content can not be empty"
        });
    }

    // Find note and update it with the request body
    WorkTemplate.findByIdAndUpdate(req.params.work_templateId, {
        action: req.body.action || [],
        time: req.body.time,
        parent: req.body.parent,
        parentId: req.body.parentId,
        name: req.body.name
    }, {new: true})
    .then(work_template => {
        if(!work_template) {
            return res.status(404).send({
                message: "WorkTemplate not found with id " + req.params.work_templateId
            });
        }
        res.send(work_template);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "WorkTemplate not found with id " + req.params.work_templateId
            });                
        }
        return res.status(500).send({
            message: "Error updating work_template with id " + req.params.work_templateId
        });
    });
};

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    WorkTemplate.findByIdAndRemove(req.params.work_templateId)
    .then(work_template => {
        if(!work_template) {
            return res.status(404).send({
                message: "WorkTemplate not found with id " + req.params.work_templateId
            });
        }
        res.send({message: "WorkTemplate deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "WorkTemplate not found with id " + req.params.work_templateId
            });
        }
        return res.status(500).send({
            message: "Could not delete work_template with id " + req.params.work_templateId
        });
    });
};