const Schedule = require('../models/schedule.model.js');
var moment = require('moment');
moment.locale('vi');
moment().format();
// Create and Save a new Note
exports.create = (req, res) => {
    if(!req.body.queue){
        return res.status(400).send({
            message: "Schedule queue of works can not be empty"
        })
    }
    if(!req.body.time_start){
        return res.status(400).send({
            message: "Add a time start for a Schedule"
        })
    }
    const schedule = new Schedule({
        queue: req.body.queue || [],
        parentID: req.body.parentID || "",
        time_start: moment().add(req.body.time_start, 'm').toDate() || moment()
    })
    schedule.save()
    .then(data => {
        res.send(data);
    })
    .catch(err =>{
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Schedule"
        })
    })
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Schedule.find()
    .then(schedules => {
        res.send(schedules);
    })
    .catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving the Schedule"
        })
    })
};

// Find a single note with a noteId
exports.findOne = (req, res) => {
    Schedule.findById(req.params.scheduleId)
    .then (schedule =>{
        if (!schedule){
            return res.status(404).send({
                message: "Schedule not found with id"
            });
        }
        res.send(schedule);
    })
    .catch(err => {
        if (err.kind === "ObjectId"){
            return res.status(404).send({
                message: "Schedule not found with Id"
            });
        }
        return res.status(500).send({
            message: "Error retrieving schedule with Id"
        });
    })
}

exports.findBy = (req, res) => {
    Schedule.find(req.params)
    .then(schedules => {
        if(!schedules){
            return res.status(404).send({
                message: "Schedule not found"
            })
        }
        res.send(schedules);
    })
    .catch(err => {
        res.status(500).send({
            message: err.message || "SSome error occurred while retrieving the Schedule"
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    if (!req.body.queue){
        return res.status(400).send({
            message: "Schedule queue of works can not be empty"
        });
    }
    Schedule.findByIdAndUpdate(req.params.scheduleId, {
        queue: req.body.queue || [],
        parentID: req.body.parentID || "",
        time_start: moment().add(req.body.time_start, 'm').toDate() || moment()
    }, {new: true})
    .then(schedule =>{
        if(!schedule){
            return res.status(404).send({
                message: "Schedule not found"
            })
        }
        res.send(schedule);
    })
    .catch(err =>{
        return res.status(500).send({
            message: "Error updating:" + err.message
        });
    });
};

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Schedule.findByIdAndRemove(req.params.scheduleId)
    .then(schedule =>{
        if(!schedule){
            return res.status(404).send({
                message: "Schedule not found, can not delete"
            })
        }
    })
};